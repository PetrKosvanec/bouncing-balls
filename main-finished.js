// UPON COLLISION W EVIL CIRCLE 2 BALLS DISAPPEAR
//   INSTEAD OF 1

// setup canvas

var canvas = document.querySelector('canvas');
var ctx = canvas.getContext('2d');

var width = canvas.width = window.innerWidth;
var height = canvas.height = window.innerHeight;

// prep variables

var para = document.querySelector('p');	// <p> displaying # balls eaten by EvilCircle
var eCsize = 10;			// EvilCircle radius
var nBallsOnStart = 25;
var allBalls;				// # balls in existence on canvas at any given moment

// function to set number of balls to nBallsOnStart - onload or onrefresh;

maxBalls = function() {
  allBalls = nBallsOnStart;
  display();
}

// function to generate random number

function random(min,max) {
  var num = Math.floor(Math.random()*(max-min)) + min;
  return num;
}

// define displaying ball count

display = function() {
  var str2displ = String.prototype.concat("Ball count: ", allBalls.toString());
  para.textContent = str2displ;
}


// define Shape constructor

function Shape(x, y, velX, velY, exists) {
	this.x = x;
	this.y = y;
	this.velX = velX;
	this.velY = velY;
	this.exists = true;
}

// define EvilCircle constructor

function EvilCircle(x, y, velX, velY, color, size, exists) {
  Shape.call(this, x, y, 20, 20, exists);
    
    this.color = 'white';
    this.size = eCsize;
}

// define evil circle draw method

EvilCircle.prototype.draw = function() {
  ctx.beginPath();
  ctx.lineWidth = 3;
  ctx.strokeStyle = this.color;
  // ctx.moveTo()
  ctx.arc(this.x, this.y, this.size, 0, 2 * Math.PI);
  ctx.stroke();
};

// define evil circle update method

EvilCircle.prototype.checkBounds = function() {
  if((this.x + this.size) >= width) {
    this.x -= this.size;
  }

  if((this.x - this.size) <= 0) {
    this.x += this.size;
  }

  if((this.y + this.size) >= height) {
    this.y -= this.size;
  }

  if((this.y - this.size) <= 0) {
    this.y += this.size;
  }
};

// define evil circle move controls
// your keyCodes map to 65 A, 68 D, 87 W, 83 S. my keyCodes map to arrows - left 37, up 38, right 39, down 40.
// this.x & this.velX are in scope of EvilCircle.prototype.setControls = function() {},
//   but they are out of scope of window.onkeydown = function(e) {}.

EvilCircle.prototype.setControls = function() {
  var _this = this;
  window.onkeydown = function(e) {
    if (e.keyCode === 37) {
	    _this.x -= _this.velX;
    }
    else if (e.keyCode === 39) {
            _this.x += _this.velX;
    }
    else if (e.keyCode === 38) {
            _this.y -= _this.velY;
    }
    else if (e.keyCode === 40) {
            _this.y += _this.velY;
    }
  }
};
////	 bckp:
//EvilCircle.prototype.setControls = function() {
//  var _this = this;
//  window.onkeydown = function(e) {
//    if (e.keyCode === 37) {
//	    _this.x -= _this.velX;
//    }
//    else if (e.keyCode === 39) {
//            _this.x += _this.velX;
//    }
//    else if (e.keyCode === 38) {
//            _this.y -= _this.velY;
//    }
//    else if (e.keyCode === 40) {
//            _this.y += _this.velY;
//    }
//  }
//};

// define evil circle collision w. balls detection

EvilCircle.prototype.collisionDetect = function() {
  for(var j = 0; j < balls.length; j++) {
    if (balls[j].exists) {
      var dx = this.x - balls[j].x;
      var dy = this.y - balls[j].y;
      var distance = Math.sqrt(dx * dx + dy * dy);

      if (distance < (this.size + balls[j].size)) {
        balls[j].exists = false;
	allBalls -= 1;
	display();
      }
    }
  }
};

// define Ball constructor

function Ball(x, y, velX, velY, color, size, exists) {
  Shape.call(this, x, y, velX, velY, exists);
  
  this.color = color;
  this.size = size;
}

// define ball draw method

Ball.prototype.draw = function() {
  ctx.beginPath();
  ctx.fillStyle = this.color;
  ctx.arc(this.x, this.y, this.size, 0, 2 * Math.PI);
  ctx.fill();
};

// define ball update method

Ball.prototype.update = function() {
  if((this.x + this.size) >= width) {
    this.velX = -(this.velX);
  }

  if((this.x - this.size) <= 0) {
    this.velX = -(this.velX);
  }

  if((this.y + this.size) >= height) {
    this.velY = -(this.velY);
  }

  if((this.y - this.size) <= 0) {
    this.velY = -(this.velY);
  }

  this.x += this.velX;
  this.y += this.velY;
};

// define ball collision detection

Ball.prototype.collisionDetect = function() {
  for(var j = 0; j < balls.length; j++) {
    if(!(this === balls[j])) {
      var dx = this.x - balls[j].x;
      var dy = this.y - balls[j].y;
      var distance = Math.sqrt(dx * dx + dy * dy);

      if (distance < this.size + balls[j].size) {
        balls[j].color = this.color = 'rgb(' + random(0,255) + ',' + random(0,255) + ',' + random(0,255) +')';
      }
    }
  }
};

// define array to store balls

var balls = [];

// define loop that keeps drawing the scene constantly

function loop() {
  ctx.fillStyle = 'rgba(0, 0, 0, 0.25)';
  ctx.fillRect(0 , 0, width, height);

  while(balls.length < allBalls) {
    var size = random(10, 20);
    var ball = new Ball(
      // ball position always drawn at least one ball width
      // away from the adge of the canvas, to avoid drawing errors
      random(0 + size, width - size),
      random(0 + size, height - size),
      random(-0.1, 0.1),
      random(-0.1, 0.1),
      'rgb(' + random(0, 255) + ',' + random(0, 255) + ',' + random(0, 255) +')',
      size,
      true
    );
    balls.push(ball);
  }
 
  evilCircle.draw();
  evilCircle.checkBounds();
  evilCircle.collisionDetect();

  for(let i = 0; i < balls.length; i++) {
    if (balls[i].exists) {
      balls[i].draw();
      balls[i].update();
      balls[i].collisionDetect();
    }
  }
  requestAnimationFrame(loop);
}

var evilCircle = new EvilCircle(random(0 + eCsize, width - eCsize), random(0 + eCsize, height - eCsize), 20, 20, 'white', eCsize, true);
evilCircle.setControls();

loop();
